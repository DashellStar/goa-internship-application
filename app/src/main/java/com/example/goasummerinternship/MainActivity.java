package com.example.goasummerinternship;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    ImageButton myImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myImageButton = (ImageButton) findViewById(R.id.Green);

        myImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLoadNewActivity = new Intent(MainActivity.this, Green.class);
                startActivity(intentLoadNewActivity);
            }
        });

        ImageButton myImageButton2;

        myImageButton2 = (ImageButton) findViewById(R.id.avatar);

        myImageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLoadNewActivity = new Intent(MainActivity.this, ProfilePicture.class);
                startActivity(intentLoadNewActivity);
            }
        });

        ImageButton myImageButton3;

        myImageButton3 = (ImageButton) findViewById(R.id.wood);

        myImageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLoadNewActivity = new Intent(MainActivity.this, Wood.class);
                startActivity(intentLoadNewActivity);
            }
        });
    }


}